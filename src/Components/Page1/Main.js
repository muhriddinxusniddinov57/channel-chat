import styled from 'styled-components';


export const Main = styled.div`

width:1300px;
margin-left:110px;
text-align:center;
height:800px;
border:none;
display:flex;
// justify-content:space-around;
`

export const Box1 = styled.div`

width:310px;
height:auto;
border:none;
margin-top:60px;
margin-left:100px;
`

export const Box2 = styled.div`

width:482px;
height:auto;
border:none;
margin-top:60px;
margin-left:30px;



`


export const  Box1Div= styled.div`
border:none;
width:300px;
height:40px;
margin:20px 0px;

&:hover{
    background-color:silver;
    border-radius:10px;
}

.router_linkNotifacation{
    list-style:none;
    text-decoration:none;
    color:black;
    margin-left: 17px;
    
    }

    .router_linkMessages{
        list-style:none;
    text-decoration:none;
    color:black;
    margin-left: 9px;
    }



`

export const Box1DivImg = styled.img`
width:30px;
height:30px;
margin-right:250px;
margin-top:3px;

`

export const Box1DivP = styled.p`
font-size:20px;
margin-left:30px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;

`

export const Box1HouseP = styled.p`
font-size:20px;
margin-left:-120px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;
`

export const Box1MembersP = styled.p`
font-size:20px;
margin-left:-90px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;


.router_link{
    list-style:none;
    text-decoration:none;
    color:black;

   

}
`
export const Box1NotificationP = styled.p`
font-size:20px;
margin-left:-73px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;
`
export const Box1MessagesP = styled.p`
font-size:20px;
margin-left:-81px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;
`


export const HR = styled.hr`
width:310px;
height:2px;

`

export const Box1BottomP1 = styled.p`

font-size:20px;
margin-right:200px;

`

export const Box1BottomP2 = styled.p`

font-size:20px;
margin-right:255px;

`

export const Box1BottomP3 = styled.p`

font-size:20px;
margin-right:235px;

`


export const Btn = styled.button`

width:200px;
height:35px;
margin-left:10px;
display:flex;
border-radius:10px;
border:none;
background-color:#156DC5;
color:white;
margin-top:30px; 


&:hover{
    background-color:#50A0EF;
    transition:0.3s;
    // padding:4px;

}


`

export const BtnImage = styled.img`
width:25px;
height:25px;
margin-left:10px;
margin-top:5px;




`
export const BtnP = styled.p`

font-size:18px;
margin-left:10px;
margin-top:3px;


`

export const Box2Div = styled.div`

width:480px;
height:65px;
border:none;
display:flex;
background-color:white;
border-radius:12px;
// box-shadow: 1px 9px 13px -8px rgba(34, 60, 80, 0.49);


`

export const Box2Input = styled.input`
width:400px;
height:40px;
border-radius:12px;
margin-left:12px;
background-color:#F0F1F2 ;
margin-top:10px;
border:none;
outline:none;
font-size:17px;


`


export const Box2Img = styled.img`

width:35px;
height:35px;
margin-left:18px;
margin-top:12px;


`

export const Box2Div2 = styled.div`

width:480px;
height:auto;
border:none;
background-color:white;
border-radius:12px;
margin-top:20px;
// box-shadow: 1px 9px 13px -8px rgba(34, 60, 80, 0.49);


`
export const Box2Div2Nav = styled.div`

border:none;
width:490px;
height:70px;
display:flex;
// margin-top:40px;


`

export const NavImg = styled.img`
width:35px;
margin-left:20px;
margin-top:18px;
height:35px;
`

export const NavP = styled.p`

font-size:20px;
color:black;
margin-top:10px;
margin-left:13px;

`

export const NavDate = styled.p`

font-size:13px;
margin-top:40px;
margin-left:-205px;

`

export const NavWrapperspan = styled.div`

width:50px;
height:30px;
border:none;
display:flex;
margin-left:230px;
margin-top:30px;

`


export const NavSpan = styled.span`

width:4px;
height:4px;
background:black;
border-radius:50%;
display:block;
margin: 0 6px;
margin-top:10px;
margin-left:2px;

`

export const ThePost = styled.p`
font-size:20px;
display:block;
margin-top:20px;
margin-right:280px;

`

export const LikeDiv = styled.div`

border-top:1px solid grey;
width:480px;
height:auto;
display:flex;
justify-content: space-around;



`

export const LikeDivWrapper = styled.div`

width:100px;
height: 50px;
border:none;
display:flex;
margin-top:20px;
// margin-left:20px;

`

export const LikeImg = styled.img`
width:30px;
margin:0 10px;
height:35px;
margin-left:10px;


`

export const LikeP = styled.p`
font-size:19px;
margin-top:3px;
`

export const LikeDivWrapper2 = styled.div`
width:140px;
height: 50px;
border:none;
display:flex;
margin-top:22px;
// margin-left:-25px;



`

export const LikeDivWrapper3 = styled.div`
width:110px;
height: 50px;
border:none;
display:flex;
margin-top:22px;
// margin-left:-25px;   



`
