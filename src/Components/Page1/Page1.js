import { Link } from "react-router-dom";
import React, { useState } from "react";
import './Container';
import './Header';
import { Image, ImgBell, ImgUser, Input, Navbar, Search, WrapperInputImage } from "./Header";
import { Container } from "./Container";
import { Box1,  Box1BottomP1, Box1BottomP2, Box1BottomP3, Box1Div, Box1DivImg, Box1DivP, Box1HouseP, Box1MembersP, Box1MessagesP, Box1NotificationP, Box2, Box2Div, Box2Div2, Box2Div2Nav, Box2Img, Box2Input,  Btn, BtnImage, BtnP, HR, LikeDiv, LikeDivWrapper, LikeDivWrapper2, LikeDivWrapper3, LikeImg, LikeP, Main, NavDate, NavImg, NavP, NavSpan, NavWrapperspan, ThePost } from "./Main";
import { Box3, Box3Members, Box3MembersP, LastMembersP, RightBoxBottom, RightBoxBottomP, RightBoxDiv, RightBoxDownP, RightBoxHr, RightBoxImg, RightBoxP, RightBoxUpP } from "./RightSIdebar";
import { Modal } from "./Modal";
import { ThemeProvider} from "styled-components";
import {Light, Dark, GlobalStyles } from "./themes"



function Page1(){

    // /Modal///
    const [showModal, setShowModal] = useState(false)

    const openModal =()=>{
      setShowModal(prev=>!prev)
    }
    // /Modal///





    return(
        <>
        <Container >
        <Navbar>

            <Image src="./image/finger.png" />
            <WrapperInputImage>
            <Search src="./image/search.png"></Search>
            <Input type="text" placeholder="Search for posts and members" ></Input>
            </WrapperInputImage>

            <ImgBell  src="image/bell.png"/>
            <ImgUser src="image/newuser.png" />
        </Navbar>

        <Main>

            <Box1>
            <Box1Div>
                <Box1DivImg src="./image/newuser.png"/>
                <Box1DivP>Muhriddin Xusniddinov</Box1DivP>
            </Box1Div>


            <Box1Div>
                <Box1DivImg src="./image/House.png"/>
                <Box1HouseP>Home</Box1HouseP>
            </Box1Div>


            <Box1Div>
                <Box1DivImg src="./image/group.png"/>
                <Box1MembersP><Link to={'/Page2'} className="router_link">Members</Link></Box1MembersP>
            </Box1Div>




            <Box1Div>
                <Box1DivImg src="./image/notification.png"/>
                <Box1MembersP><Link to={'/Page3'} className="router_linkNotifacation">Notification</Link></Box1MembersP>

            </Box1Div>



            <Box1Div>
                <Box1DivImg src="./image/message.png"/>
                <Box1MembersP><Link to={'/Page4'} className="router_linkMessages">Messages</Link></Box1MembersP>

            </Box1Div>
        
        <HR></HR>
<Box1BottomP1>Java script</Box1BottomP1>
<Box1BottomP2>CSS</Box1BottomP2>
<Box1BottomP3>HTML</Box1BottomP3>

<Btn  className="btnCreateChanel" onClick={openModal}><BtnImage src="./image/noise1.png"/><BtnP>Create Channel</BtnP></Btn>
<Modal showModal={showModal} setShowModal={setShowModal}></Modal>


            </Box1>



            
            <Box2>

            <Box2Div className="shadow">
                <Box2Img src="./image/newuser.png"  />
                <Box2Input placeholder="What do you want to talk about?" />
            </Box2Div>

            <Box2Div2 className="shadow">

                
                <Box2Div2Nav>
                    <NavImg src="image/newuser.png" />
                    <NavP>Muhriddin Xusniddinov</NavP>
                    <NavDate>28 day ago-Java script </NavDate>
                    <NavWrapperspan>
                        <NavSpan></NavSpan>
                        <NavSpan></NavSpan>
                        <NavSpan></NavSpan>

                    </NavWrapperspan>
                </Box2Div2Nav>
            <ThePost>The post</ThePost>
        
            <LikeDiv>


                <LikeDivWrapper>
                <LikeImg src="image/like.png" />
                <LikeP>Like</LikeP>
                </LikeDivWrapper>


                <LikeDivWrapper2>
                <LikeImg src="image/comment.png.png" />
                <LikeP>Message</LikeP>
                </LikeDivWrapper2>


                
                <LikeDivWrapper3>
                <LikeImg src="image/send1.png" />
                <LikeP>Share</LikeP>
                </LikeDivWrapper3>
                
            </LikeDiv>

    
           </Box2Div2>



            </Box2>

            <Box3>

                <Box3Members>
                 
                 <Box3MembersP>New Members</Box3MembersP>
         

                </Box3Members>


                <RightBoxDiv>
                    <RightBoxImg src="./image/newuser.png" />
                    <RightBoxP>Doston</RightBoxP>
                </RightBoxDiv>

                <RightBoxDiv>
                    <RightBoxImg src="./image/newuser.png" />
                    <RightBoxP>Afzal</RightBoxP>
                </RightBoxDiv>

                <RightBoxDiv>
                    <RightBoxImg src="./image/newuser.png" />
                    <RightBoxP>Sobir</RightBoxP>
                </RightBoxDiv>
<RightBoxUpP>Online Members</RightBoxUpP>
<RightBoxHr />
<RightBoxDownP>No members are online</RightBoxDownP>


            </Box3>
            
        </Main>






        </Container>

        
        
        </>
    )
}

export default Page1;