import styled from "styled-components";


export const Box3 = styled.div`

width:310px;
height:700px;
margin-top:50px;
border:none;
margin-left:30px;    


`


export const Box3Members = styled.div`
width:270px;
border-bottom:1px solid grey;
height:40px;


`
export const Box3MembersP = styled.p`

font-size:20px;
margin-right:90px;


`

export const RightBoxDiv = styled.div`

border:none;
width:260px;
height:40px;
display:flex;
margin-left:15px;
margin-top:20px;


&:hover{
    background-color:silver;
    border-radius:10px;
}
`


export const RightBoxImg = styled.img`

width:35px;
height:35px;

`

export const RightBoxP = styled.p`

font-size:20px;
margin-left:10px;

`


export const RightBoxUpP = styled.p`
font-size:20px;
margin-left:-120px;
margin-top:40px;

`

export const RightBoxHr = styled.div`
width:300px;
border: 1px solid silver;
/* position:fixed; */
`

export const RightBoxDownP = styled.p`
font-size:20px;
margin-left:-60px;

`