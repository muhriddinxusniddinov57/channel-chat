import styled from 'styled-components';


export const Main = styled.div`

width:1300px;
margin-left:110px;
text-align:center;
height:800px;
border:none;
display:flex;
`

export const Box1 = styled.div`

width:310px;
height:auto;
border:none;
margin-top:60px;
margin-left:100px;
`

export const Box2 = styled.div`

width:482px;
height:auto;
border:none;
margin-top:60px;
margin-left:30px;



`


export const  Box1Div= styled.div`
border:none;
width:300px;
height:40px;
margin:20px 0px;

&:hover{
    background-color:silver;
    border-radius:10px;

   
}

.router_linkNotification{
        list-style: none;
        text-decoration: none;
        color: black;
    }


    .router_linkMessages{
        list-style: none;
        text-decoration: none;
        color: black;
        margin-right: 9px;
    }



`

export const Box1DivImg = styled.img`
width:30px;
height:30px;
margin-right:250px;
margin-top:3px;

`

export const Box1DivP = styled.p`
font-size:20px;
margin-left:30px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;

`

export const Box1HouseP = styled.p`
font-size:20px;
margin-left:-120px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;
.router_link{
    list-style:none;
    text-decoration:none;
    color:black;

}
`

export const Box1MembersP = styled.p`
font-size:20px;
margin-left:-90px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;


.router_link{
    list-style:none;
    text-decoration:none;
    color:black;

}
`
export const Box1NotificationP = styled.p`
font-size:20px;
margin-left:-73px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;
`
export const Box1MessagesP = styled.p`
font-size:20px;
margin-left:-81px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;
`


export const HR = styled.hr`
width:310px;
height:2px;

`

export const Box1BottomP1 = styled.p`

font-size:20px;
margin-right:200px;

`

export const Box1BottomP2 = styled.p`

font-size:20px;
margin-right:255px;

`

export const Box1BottomP3 = styled.p`

font-size:20px;
margin-right:235px;

`


export const Btn = styled.button`

width:200px;
height:35px;
margin-left:10px;
display:flex;
border-radius:10px;
border:none;
background-color:#156DC5;
color:white;
margin-top:30px; 


&:hover{
    background-color:#50A0EF;
    transition:0.3s;
    // padding:4px;

}


`

export const BtnImage = styled.img`
width:25px;
height:25px;
margin-left:10px;
margin-top:5px;




`
export const BtnP = styled.p`

font-size:18px;
margin-left:10px;
margin-top:3px;


`

export const MainBlock = styled.div`

width:9000px;
height:600px;
border:none;
display:flex ;
`

export const Box = styled.div`
border:1px solid black ;
width:180px;
height:280px;
margin-left:33px ;
margin-top:50px;
border-radius:10px;
border:none;
background-color: #fff;


`

export const BoxIncludes = styled.div`

width: 180px;
border: none;
height:140px ;


.circle{
    width:100px;
    height:100px ;
    border-radius:50%;
    background:#C70039 ;
    text-align:center ;
    margin-left:40px;
    margin-top:30px ;
    padding:20px ;

    .circleText{
        font-size: 35px;
        text-align:center;
        color:wheat;
    }
}

.circleGreen{
    width:100px;
    height:100px ;
    border-radius:50%;
    background:#28B8A4 ;
    text-align:center ;
    margin-left:40px;
    margin-top:30px ;
    padding:20px ;

    .circleText{

        font-size:35px ;
        text-align:center;
        color:white;
    }
}



`
export const BoxIncludes2 = styled.div`
width: 180px;
border:none;
height:140px ;


.wrapperName{
        width:180px ;
        height:60px ;
        border:none;
        display: flex;
        flex-wrap:wrap ;
        overflow: hidden ;
        margin-top:-5px ;

    }

.circleName{
    font-size:20px ;
    margin-top:0px ;
    text-align:center;
    margin-left:8px ;

}
.btnBox{
    background-color:wheat;
    width:80px ;
    height:30px ;
    border:1px solid black;
    background-color:white ;
    border-radius:10px ;
    margin-top:10px ;
    :hover{
        background-color:black;
        color:white ;
    }

   
}
`
