import { Link } from "react-router-dom";
import React, { useState } from "react";
import { Image, ImgBell, ImgUser, Input, Navbar, Search, WrapperInputImage } from "./Header";
import { Container } from "./Container";
import { Box, Box1,  Box1BottomP1, Box1BottomP2, Box1BottomP3, Box1Div, Box1DivImg, Box1DivP, Box1HouseP, Box1MembersP, Box1MessagesP, Box1NotificationP, Box2, Box2Div, Box2Div2, Box2Div2Nav, Box2Img, Box2Input,  BoxIncludes,  BoxIncludes2,  Btn, BtnImage, BtnP, HR, LikeDiv, LikeDivWrapper, LikeDivWrapper2, LikeDivWrapper3, LikeImg, LikeP, Main, MainBlock, NavDate, NavImg, NavP, NavSpan, NavWrapperspan, ThePost } from "./Main";
import { Modal } from "./Modal";



function Page1(){

    const [showModal, setShowModal] = useState(false)

    const openModal =()=>{
      setShowModal(prev=>!prev)
    }
    return(
        <>
        <Container>
        <Navbar>

            <Image src="./image/finger.png" />
            <WrapperInputImage>
            <Search src="./image/search.png"></Search>
            <Input type="text" placeholder="Search for posts and members" ></Input>
            </WrapperInputImage>

            <ImgBell  src="image/bell.png"/>
            <ImgUser src="image/newuser.png" />
        </Navbar>

        <Main>

            <Box1>
            <Box1Div>
                <Box1DivImg src="./image/newuser.png"/>
                <Box1DivP>Muhriddin Xusniddinov</Box1DivP>
            </Box1Div>


            <Box1Div>
                <Box1DivImg src="./image/House.png"/>
                <Box1HouseP><Link to={'/'} className="router_link">Home</Link></Box1HouseP>
            </Box1Div>


            <Box1Div>
                <Box1DivImg src="./image/group.png"/>
                <Box1MembersP><Link to={'/Page2'} className="router_link">Members</Link></Box1MembersP>
            </Box1Div>




            <Box1Div>
                <Box1DivImg src="./image/notification.png"/>
                <Box1NotificationP><Link to={'/Page3'} className="router_linkNotification">Notification</Link></Box1NotificationP>
            </Box1Div>



            <Box1Div>
                <Box1DivImg src="./image/message.png"/>
                <Box1NotificationP><Link to={'/Page4'} className="router_linkMessages">Messages</Link></Box1NotificationP>

            </Box1Div>
        
        <HR></HR>
<Box1BottomP1>Java script</Box1BottomP1>
<Box1BottomP2>CSS</Box1BottomP2>
<Box1BottomP3>HTML</Box1BottomP3>

<Btn  className="btnCreateChanel" onClick={openModal}><BtnImage src="./image/noise1.png"/><BtnP>Create Channel</BtnP></Btn>
<Modal showModal={showModal} setShowModal={setShowModal}></Modal>

            </Box1>


    

            <MainBlock>
                    <Box className="shadow">
                        
                        <BoxIncludes>
                            <div className="circle">
                                <p className="circleText">T</p>
                                </div>
                        </BoxIncludes>
                        <BoxIncludes2>
                        <div className="wrapperName">
                           <p className="circleName">Ulug'bek Tojiboyev  </p>
                           </div> 
                        <button className="btnBox">Follow</button>
                        </BoxIncludes2>
                        
                        </Box>
                    <Box className="shadow">
                        
                    <BoxIncludes>
                            <div className="circleGreen">
                                <p className="circleText">T</p>
                                </div>
                        </BoxIncludes>
                        <BoxIncludes2>
                       <div className="wrapperName">
                           <p className="circleName">Muhriddin Xusniddinov </p>
                           </div> 
                        <button className="btnBox">Follow</button>
                        </BoxIncludes2>
                        
                        </Box>
                    <Box className="shadow">
                        
                    <BoxIncludes>
                            <div className="circleGreen">
                                <p className="circleText">T</p>
                                </div>
                        </BoxIncludes>
                        <BoxIncludes2>
                        <div className="wrapperName">
                           <p className="circleName">Muhriddin Xusniddinov </p>
                           </div> 
                        <button className="btnBox">Follow</button>
                        </BoxIncludes2>
                        
                        </Box>
                    <Box className="shadow">
                        
                    <BoxIncludes>
                            <div className="circleGreen">
                                <p className="circleText">T</p>
                                </div>
                        </BoxIncludes>
                        <BoxIncludes2>
                        <div className="wrapperName">
                           <p className="circleName">Muhriddin Xusniddinov </p>
                           </div> 
                        <button className="btnBox">Follow</button>
                        </BoxIncludes2>
                        
                        
                        </Box>




        </MainBlock>
            
        </Main>


     




        </Container>

      
        
        
        </>
    )
}

export default Page1;