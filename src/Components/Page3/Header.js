import styled from "styled-components"


export const Navbar = styled.div`
width:1512px;
height:70px;
border:none;
display:flex;
background-color:white;
box-shadow: 0px 13px 8px -8px rgba(34, 60, 80, 0.2);

`

export const WrapperInputImage = styled.div`
width:350px;
height:40px;
background-color:#F0F1F2 ;
border-radius:8px;
outline:none;
font-size:17px;
border:none;
display:flex;
margin-left:35px;
margin-top:12px;
`

export const Input = styled.input`

width:225px;
height:40px;
border:none;
background-color:#F0F1F2 ;
border-radius:8px;
outline:none;
font-size:17px;
margin-left:10px;
color:black;

`

export const Search = styled.img`

width:17px;
height:17px;
margin-left:15px;
margin-top:12px;
`

export const Image = styled.img`

width:35px;
height:40px;
margin-left:25px;
margin-top:12px;

`

export const ImgBell = styled.img`
width:27px;
height:27px;
margin-left:925px;
margin-top:16px;


`
export const ImgUser = styled.img`
width:30px;
height:30px;
margin-left:30px;
margin-top:15px;

`
