import { Link } from "react-router-dom";
import React, { useState } from "react";
import './Modal'
import { Image, ImgBell, ImgUser, Input,  Navbar, Search, WrapperInputImage } from "./Header";
import { Container } from "./Container";
import {  Box1,  Box1BottomP1, Box1BottomP2, Box1BottomP3, Box1Div, Box1DivImg, Box1DivP, Box1HouseP, Box1MembersP, Box1MessagesP, Box1NotificationP,  Btn, BtnImage, BtnP, HR,  Main, MainBlock,  } from "./Main";
import { Modal } from "./Modal";



function Page1(){
    const [showModal, setShowModal] = useState(false)

    const openModal =()=>{
      setShowModal(prev=>!prev)
    }
    
    return(
        <>
        <Container>
        <Navbar>

            <Image src="./image/finger.png" />
            <WrapperInputImage>
            <Search src="./image/search.png"></Search>
            <Input type="text" placeholder="Search for posts and members" ></Input>
            </WrapperInputImage>

            <ImgBell  src="image/bell.png"/>
            <ImgUser src="image/newuser.png" />
        </Navbar>

        <Main>

            <Box1>
            <Box1Div>
                <Box1DivImg src="./image/newuser.png"/>
                <Box1DivP>Muhriddin Xusniddinov</Box1DivP>
            </Box1Div>


            <Box1Div>
                <Box1DivImg src="./image/House.png"/>
                <Box1HouseP><Link to={'/'} className="router_link">Home</Link></Box1HouseP>
            </Box1Div>


            <Box1Div>
                <Box1DivImg src="./image/group.png"/>
                <Box1MembersP><Link to={'/Page2'} className="router_link">Members</Link></Box1MembersP>
            </Box1Div>




            <Box1Div>
                <Box1DivImg src="./image/notification.png"/>
                <Box1NotificationP>Notification</Box1NotificationP>
            </Box1Div>



            <Box1Div>
                <Box1DivImg src="./image/message.png"/>
                <Box1MessagesP><Link to={'/Page4'} className="router_linkMessages">Messages</Link></Box1MessagesP>
            </Box1Div>
        
        <HR></HR>
<Box1BottomP1>Java script</Box1BottomP1>
<Box1BottomP2>CSS</Box1BottomP2>
<Box1BottomP3>HTML</Box1BottomP3>


<Btn  className="btnCreateChanel" onClick={openModal}><BtnImage src="./image/noise1.png"/><BtnP>Create Channel</BtnP></Btn>
<Modal showModal={showModal} setShowModal={setShowModal}></Modal>

            </Box1>

            <MainBlock className="shadow">
                <div className="Box">
                <div >
                    <img className="boxImg" src="./image/newuser.png" />
                    </div>
                <div className="boxText">
                    <p className="text">
                        Alisher send messaded  you
                        </p>
                    </div>
                </div>




                <div className="Box">
                <div >
                    <img className="boxImg" src="./image/newuser.png" />
                    </div>
                <div className="boxText">
                    <p className="text">
                        Alisher send messaded  you
                        </p>
                    </div>
                </div>





                <div className="Box">
                <div >
                    <img className="boxImg" src="./image/newuser.png" />
                    </div>
                <div className="boxText">
                    <p className="text">
                        Alisher send messaded  you
                        </p>
                    </div>
                </div>





                <div className="Box">
                <div >
                    <img className="boxImg" src="./image/newuser.png" />
                    </div>
                <div className="boxText">
                    <p className="text">
                        Alisher send messaded  you
                        </p>
                    </div>
                </div>




                <div className="Box">
                <div >
                    <img className="boxImg" src="./image/newuser.png" />
                    </div>
                <div className="boxText">
                    <p className="text">
                        Alisher send messaded  you
                        </p>
                    </div>
                </div>
            </MainBlock>
    
    

    
  

        </Main>


     




        </Container>

      
        
        
        </>
    )
}

export default Page1;