import styled from 'styled-components';


export const Main = styled.div`

width:1300px;
margin-left:110px;
text-align:center;
height:800px;
border:none;
display:flex;
`

export const Box1 = styled.div`

width:310px;
height:auto;
border:none;
margin-top:60px;
margin-left:100px;
`

export const Box2 = styled.div`

width:482px;
height:auto;
border:none;
margin-top:60px;
margin-left:30px;



`


export const  Box1Div= styled.div`
border:none;
width:300px;
height:40px;
margin:20px 0px;

&:hover{
    background-color:silver;
    border-radius:10px;
}

.router_linkNotification{
    text-decoration: none;
    margin-left: 17px;
    color: black;
}



`

export const Box1DivImg = styled.img`
width:30px;
height:30px;
margin-right:250px;
margin-top:3px;

`

export const Box1DivP = styled.p`
font-size:20px;
margin-left:30px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;

`

export const Box1HouseP = styled.p`
font-size:20px;
margin-left:-120px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;
.router_link{
    list-style:none;
    text-decoration:none;
    color:black;

}
`

export const Box1MembersP = styled.p`
font-size:20px;
margin-left:-90px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;


.router_link{
    list-style:none;
    text-decoration:none;
    color:black;

}
`
export const Box1NotificationP = styled.p`
font-size:20px;
margin-left:-73px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;
`
export const Box1MessagesP = styled.p`
font-size:20px;
margin-left:-81px;
flex-wrap: nowrap;
display:block;
margin-top:-28px;
font-family: 'Roboto', sans-serif;
font-style:normal;
`


export const HR = styled.hr`
width:310px;
height:2px;

`

export const Box1BottomP1 = styled.p`

font-size:20px;
margin-right:200px;

`

export const Box1BottomP2 = styled.p`

font-size:20px;
margin-right:255px;

`

export const Box1BottomP3 = styled.p`

font-size:20px;
margin-right:235px;

`


export const Btn = styled.button`

width:200px;
height:35px;
margin-left:10px;
display:flex;
border-radius:10px;
border:none;
background-color:#156DC5;
color:white;
margin-top:30px; 


&:hover{
    background-color:#50A0EF;
    transition:0.3s;
    // padding:4px;

}


`

export const BtnImage = styled.img`
width:25px;
height:25px;
margin-left:10px;
margin-top:5px;

`
export const BtnP = styled.p`
font-size:18px;
margin-left:10px;
margin-top:3px;

`

export const MainBlock = styled.div`
width:800px;
height:660px ;
margin-left:30px ;
background-color:#ffff ;
`
export const MainBlock1 = styled.div`
border:none;
`
export const MainBlock2 = styled.div`
border:1px solid silver;
border-right:none;
border-bottom:none;
`

export const MainBlock1Nav1 = styled.div`
width:202px;
height: 50px;
border:1px solid silver ;
border-bottom:none;
border-left:none;
margin-left:-13px ;
`

export const MainBlock2Nav2 = styled.div`
width:600px;
height: 50px;
margin-left:-13px ;
border:1px solid silver;
border-left:none;
border-right:none;
border-top:none ;
display: flex;
`
export const MainBlockNav2Img = styled.img`
width:40px;
height:40px ;
margin-left:-45px;
margin-top:3px ;
/* position: relative; */

`


export const MainBlock2Nav2P = styled.p`
font-size:25px ;
margin-left:7px ;
margin-top:2px ;
`

export const MainBlock2Nav2Span = styled.span`
display:block ;
width:15px;
height:15px;
border-radius:50%;
background-color: #5DE12D;
margin-left: 40px;
margin-top: 30px;
`

export const MainBlock1Nav1P = styled.p`
font-size:22px ;
margin-left:-80px ;
margin-top:5px ;
align-items:center ;
`

export const MainBlockWrapper = styled.div`
border:1px solid silver ;
border-right:none;
border-left:none;
width:200px;
height:40px;
margin-left:-12px ;
display: flex;
flex-wrap:wrap ;
`

export const MainBlock1Input = styled.input`
width:165px;
height:38px ;
margin-left:32px ;
font-size:17px ;
border:none ;
outline:none ;

`

export const MainBlockImage = styled.img`

width:17px;
height:17px ;
margin-top:-28px;
margin-left:10px ;
`

export const MainBlock1Box = styled.div`
border:none;
width:175px;
border-radius:10px;
height:50px;
margin-top:15px;
display: flex;
:hover{
    background-color:#50A0EF;
    color: #ffff
}
`
export const MainBlock1BoxImg = styled.img`
width:40px;
height:40px ;
margin-top:3px ;
`

export const MainBlock1WrapperBox = styled.div`

width:180px;
height:50px;
display: flex;
flex-wrap:wrap ;
padding: 0 5px ;
border:none ;
`

export const MainBlock1Boxh3 = styled.h4`
font-size: 19px;
margin-left:5px ;
margin-bottom:3px ;

`
export const MainBlock1Boxp = styled.p`
margin-top:-20px ;
margin-left:48px ;

`

export const MainBlock2Input = styled.input`
border-radius:10px;
height:45px ;
width:450px ;
margin-left:15px ;
background-color:#ffffff;
border:none;
outline:none;
font-style:initial ;
font-size:20px ;
/* color:wheat ; */
/* padding: 30 30px ; */
`
export const MainBlock2WrapperInput = styled.div`
width:580px;
height:50px;
border:1px solid silver ;
display: flex;
margin-top:535px;
border-radius:10px;


`

export const MainBlockSend = styled.img`
width:30px ;
height:30px ;
margin-top:7px ;
margin-right: 10px;
`

export const MainBlockPencil = styled.img`
width:30px;
height:30px ;
margin-top:10px ;
margin-left:15px ;
`