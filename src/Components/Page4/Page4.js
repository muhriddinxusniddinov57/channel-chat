import { Link } from "react-router-dom";
import React, { useState } from "react";
import './Modal'
import { Image, ImgBell, ImgUser, Input,  Navbar, Search, WrapperInputImage } from "./Header";
import { Container } from "./Container";
import {  Box1,  Box1BottomP1, Box1BottomP2, Box1BottomP3, Box1Div, Box1DivImg, Box1DivP, Box1HouseP, Box1MembersP, Box1MessagesP, Box1NotificationP,  Btn, BtnImage, BtnP, HR,  Main, MainBlock, MainBlock1, MainBlock1Box, MainBlock1Boxh3, MainBlock1BoxImg, MainBlock1Boxp, MainBlock1Input, MainBlock1Nav1, MainBlock1Nav1P, MainBlock1WrapperBox, MainBlock2, MainBlock2Input, MainBlock2Nav2, MainBlock2Nav2P, MainBlock2Nav2Span, MainBlock2WrapperInput, MainBlockImage, MainBlockNav2Img, MainBlockPencil, MainBlockSend, MainBlockWrapper,  } from "./Main";
import { Modal } from "./Modal";



function Page1(){


    const [showModal, setShowModal] = useState(false)

    const openModal =()=>{
      setShowModal(prev=>!prev)
    }

    return(
        <>
        <Container>
        <Navbar>

            <Image src="./image/finger.png" />
            <WrapperInputImage>
            <Search src="./image/search.png"></Search>
            <Input type="text" placeholder="Search for posts and members" ></Input>
            </WrapperInputImage>

            <ImgBell  src="image/bell.png"/>
            <ImgUser src="image/newuser.png" />
        </Navbar>

        <Main>

            <Box1>
            <Box1Div>
                <Box1DivImg src="./image/newuser.png"/>
                <Box1DivP>Muhriddin Xusniddinov</Box1DivP>
            </Box1Div>


            <Box1Div>
                <Box1DivImg src="./image/House.png"/>
                <Box1HouseP><Link to={'/'} className="router_link">Home</Link></Box1HouseP>
            </Box1Div>


            <Box1Div>
                <Box1DivImg src="./image/group.png"/>
                <Box1MembersP><Link to={'/Page2'} className="router_link">Members</Link></Box1MembersP>
            </Box1Div>




            <Box1Div>
                <Box1DivImg src="./image/notification.png"/>
                <Box1MembersP><Link to={'/Page3'} className="router_linkNotification">Notification</Link></Box1MembersP>


            </Box1Div>



            <Box1Div>
                <Box1DivImg src="./image/message.png"/>
                <Box1MessagesP>Messages</Box1MessagesP>
            </Box1Div>
        
        <HR></HR>
<Box1BottomP1>Java script</Box1BottomP1>
<Box1BottomP2>CSS</Box1BottomP2>
<Box1BottomP3>HTML</Box1BottomP3>


<Btn  className="btnCreateChanel" onClick={openModal}><BtnImage src="./image/noise1.png"/><BtnP>Create Channel</BtnP></Btn>
<Modal showModal={showModal} setShowModal={setShowModal}></Modal>


            </Box1>



{/* //////////////Main Blok ////////// */}


<MainBlock className="row shadow" >
    <MainBlock1 className="col-3">
        <MainBlock1Nav1><MainBlock1Nav1P>Messages</MainBlock1Nav1P></MainBlock1Nav1>
      <MainBlockWrapper>
      <MainBlock1Input placeholder="Search members" />
      <MainBlockImage src="./image/search.png" />
      </MainBlockWrapper>
      <MainBlock1Box>
    <MainBlock1WrapperBox>
       <MainBlock1BoxImg src="./image/newuser.png" />
        <MainBlock1Boxh3>Muhriddin</MainBlock1Boxh3>
        <MainBlock1Boxp>Hello</MainBlock1Boxp>
    </MainBlock1WrapperBox>        


      </MainBlock1Box>
      <MainBlock1Box>
      <MainBlock1WrapperBox>
       <MainBlock1BoxImg src="./image/newuser.png" />
        <MainBlock1Boxh3>Khabib</MainBlock1Boxh3>
        <MainBlock1Boxp>I send message</MainBlock1Boxp>
    </MainBlock1WrapperBox>        
      </MainBlock1Box>


      <MainBlock1Box>
      <MainBlock1WrapperBox>
       <MainBlock1BoxImg src="./image/newuser.png" />
        <MainBlock1Boxh3>Zubayr</MainBlock1Boxh3>
        <MainBlock1Boxp>I go to Paris</MainBlock1Boxp>
    </MainBlock1WrapperBox>        
      </MainBlock1Box>
      
        </MainBlock1>


    <MainBlock2 className="col-9">
        <MainBlock2Nav2>
            <MainBlock2Nav2Span />
            <MainBlockNav2Img src="./image/newuser.png" />
            <MainBlock2Nav2P>Muhriddin</MainBlock2Nav2P>
        </MainBlock2Nav2>
            <MainBlock2WrapperInput>
                <MainBlockPencil src="./image/smiling.png" />
        <MainBlock2Input placeholder="Message" />
<MainBlockSend src="./image/send2.png"/>
            </MainBlock2WrapperInput>
        </MainBlock2>
</MainBlock>
           


  

               
        </Main>


     




        </Container>

      
        
        
        </>
    )
}

export default Page1;