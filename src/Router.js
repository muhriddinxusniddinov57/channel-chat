import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Page1 from './Components/Page1/Page1';
import Page2 from './Components/Page2/Page2'
import Page3 from './Components/Page3/Page3'
import Page4 from './Components/Page4/Page4'



class Rout extends React.Component{

    render(){
        return(
            <>
            <Routes>
                <Route path='/' element={<Page1/>}></Route>
                <Route path='/Page2' element={<Page2/>}></Route>
                <Route path='/Page3' element={<Page3/>}></Route>
                <Route path='/Page4' element={<Page4/>}></Route>

            </Routes>
      
            </>
        );
        };
        

        
}


export default Rout;